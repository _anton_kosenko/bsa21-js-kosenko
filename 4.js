// Напишіть функцію, яка обчислює подвійний факторіал числа Приклад:
// doubleFactorial(0) ➞ 1
// doubleFactorial(2) ➞ 2
// doubleFactorial(9) ➞ 945 // 97531 = 945
// doubleFactorial(14) ➞ 645120

let x = 5
let result = 0

function doubleFactorial(x) {
   if (x == 0 || x == 1) {
      return 1
   }
   else {
      return x * doubleFactorial(x - 2)
   }
}

console.log('doubleFactorial(' + x + ') -> ' + doubleFactorial(x))

x = 0
console.log('doubleFactorial(' + x + ') -> ' + doubleFactorial(x))

x = 8
console.log('doubleFactorial(' + x + ') -> ' + doubleFactorial(x))