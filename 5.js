// Дано два об’єкти person, в них містяться поля name и age.
// 5.1 Написати функцію compareAge, яка буде порівнювати два об’єкти за віком і повертати шаблонний рядок як результат. 
//Шаблонний рядок виглядає наступним чином: <Person 1 name> is <older | younger | the same age as> <Person 2 name>
// 5.2 Створити масив із декількох об’єктів person (3-5 елементів) і відсортувати його за віком: а) за зростанням б) за спаданням

const user1 = {
   name: "Andy",
   age: 14
}

const user2 = {
   name: "Sam",
   age: 15
}

console.log('First task: ')

if (user1.age === 0 || user2.age === 0) {
   console.log('Age cannot be 0!')
}
else if (user1.age > user2.age) {
   console.log(user1.name + ' is older then ' + user2.name)
}
else if (user1.age < user2.age) {
   console.log(user1.name + ' is younger then ' + user2.name)
}
else if (user1.age === user2.age) {
   console.log(user1.name + ' is the same age as ' + user2.name)
}
else {
   console.log('Invalid data!')
}

console.log('')
console.log('Second task: ')

const persons = [
   { name: "Mary", age: 35 },
   { name: "Sabrina", age: 60 },
   { name: "Andy", age: 20 },
   { name: "Linda", age: 22 },
   { name: "Tiffany", age: 45 },
]

persons.sort()

persons.sort(decSort)

function decSort(a, b) {
   if (a.age > b.age)
      return 1
   if (a.age < b.age)
      return -1
   return 0
}

console.log('Case A: increase sorting: ')
console.log(persons)

persons.sort(incSort)

function incSort(a, b) {
   if (a.age < b.age)
      return 1
   if (a.age > b.age)
      return -1
   return 0
}

console.log('Case B: decrease sorting: ')
console.log(persons)