// Дано масив, в якому є елементи true і false. Написати функцію,яка порахує кількість елементів true у масиві.
// Приклад:
// countTrue([true, false, false, true, false]) ➞ 2
// countTrue([false, false, false, false]) ➞ 0
// countTrue([]) ➞ 0

a = ["true", "false", "false", "true", "false"]
let trueCounter = 0, falseCounter = 0

function countTrue(a, trueCounter) {
   for (let i = 0; i < a.length; i++) {
      if (a[i] === "true") {
         trueCounter++
      }
   }
   return trueCounter
}

function countFalse(a, falseCounter) {
   for (let i = 0; i < a.length; i++) {
      if (a[i] === "false") {
         falseCounter++
      }
   }
   return falseCounter
}

trueCounter = countTrue(a, trueCounter)
falseCounter = countFalse(a, falseCounter)

console.log('a = [' + a + ']')
console.log('Number of true elements: ' + trueCounter)
console.log('Number of false elements: ' + falseCounter)