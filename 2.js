// Є два масиви випадкової довжини заповнених випадковими числами. Вам необхідно написати функцію, яка обчислює суму всіх елементів обох масивів.
// Приклад:
// a = [1, 2, 3, -5, 0, 10], b = [5, -1, 7] -> 22

let a = [1, 2, 3, -5, 0, 10]
let b = [5, -1, 7]
let i = 0
var result = 0

function sum(a, b) {
   for (i = 0; i < a.length; i++) {
      result = result + a[i]
   }
   i = 0
   for (i = 0; i < b.length; i++) {
      result = result + b[i]
   }
   return result
}

result = sum(a, b)
console.log('a = [' + a + '], ' + 'b = [' + b + '] -> ' + result)