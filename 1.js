// Напишіть код, який буде присвоювати змінній "result" значення суми змінних "x" та "y" - у випадку якщо x < y, 
// різницю "x" и "y" - у випадку якщо x > y, та їх добуток у решті випадків.
// Приклад:
// x = 10, y = 20 -> result = 30
// x = 20, y = 10 -> result = 10
// x = 10, y = 10 -> result = 100

function addVars(x, y) {
   return x + y
}

function subVars(x, y) {
   return x - y
}

function mulVars(x, y) {
   return x * y
}

function compare(x, y) {
   let result = 0
   if (x < y) {
      result = addVars(x, y)
   }
   else if (x > y) {
      result = subVars(x, y)
   }
   else if (x === y) {
      result = mulVars(x, y)
   }
   else {
      console.log('Invalid type of variables!')
   }
   return result
}

let x = 10, y = 20
let result = compare(x, y)

console.log('x = ' + x + ', y = ' + y + ' -> result = ' + result)

x = 20, y = 10
result = compare(x, y)

console.log('x = ' + x + ', y = ' + y + ' -> result = ' + result)

x = 10, y = 10
result = compare(x, y)

console.log('x = ' + x + ', y = ' + y + ' -> result = ' + result)

